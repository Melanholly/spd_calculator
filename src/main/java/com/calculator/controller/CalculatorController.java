package com.calculator.controller;

import com.calculator.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
//@Scope("session")
//@Secured("ROLE_USER")
@RequestMapping("/calculator")
public class CalculatorController {

    @Autowired
    private CalculatorService calculator;

    //Передаем значения и операцию
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    double process(@RequestParam String firstNum, @RequestParam String secNum, @RequestParam String action) {
        double first = Double.parseDouble(firstNum);
        double second = Double.parseDouble(secNum);

        return calculator.calculate(first, second, action);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "calculator";
        }
}

