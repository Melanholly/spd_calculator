package com.calculator.controller;

import com.calculator.service.Questions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class QuestionsController {

    @RequestMapping("/survey")
    public ModelAndView showQuestions() {
        ModelAndView mav = new ModelAndView("/survey");
        mav.addObject("questions", Questions.createQuestionsList());

        return mav;
    }

}