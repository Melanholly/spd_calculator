package com.calculator.service;

import org.springframework.stereotype.Service;

@Service
//@Scope("session")
public class CalculatorServiceImpl implements CalculatorService {

    @Override
    public double calculate(double firstNum, double secNum, String action) {

        switch (action) {
            case "mult":
                return firstNum * secNum;
            case "div":
                return firstNum / secNum;
            case "plus":
                return firstNum + secNum;
            case "minus":
                return firstNum - secNum;
            default:
                return 0;
        }

    }
}