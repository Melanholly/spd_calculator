package com.calculator.service;

public interface CalculatorService {

    public double calculate(double firstNum, double secNum, String action);
}