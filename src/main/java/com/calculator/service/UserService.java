package com.calculator.service;

import com.calculator.entity.User;

/*
Интерфейс для реализации сервиса для получения доступа к User

 */

public interface UserService {

    User getUser(String login);

}