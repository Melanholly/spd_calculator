package com.calculator.service;

import java.util.ArrayList;

public class Questions {
    private String type;
    private String question;
    private String[] answers;

    public Questions(String type, String question, String[] answers) {
        this.type = type;
        this.question = question;
        this.answers = answers;
    }

    public Questions(String type, String question) {
        this.type = type;
        this.question = question;
    }

    public String getType() {
        return type;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getAnswers() {
        return answers;
    }

    public static ArrayList createQuestionsList(){
        ArrayList questions = new ArrayList();

        Questions question1 = new Questions("radioButton", "RadioButtons", new String[]{"RadioButtons1", "RadioButtons2", "RadioButtons3"});
        Questions question2 = new Questions("checkBox", "CheckBoxes?", new String[]{"CheckBox1", "CheckBox2", "CheckBox3"});
        Questions question3 = new Questions("text", "Тут может быть ваш текст:");

        questions.add(question1);
        questions.add(question2);
        questions.add(question3);

        return questions;
    }
}