package com.calculator.dao.impl;

import com.calculator.dao.UserDao;
import com.calculator.entity.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    @Autowired
    SessionFactory sessionFactory;


    @Override
    public User getUser(String login) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where login = :login");
        query.setParameter("login", login);
        List<User> users = query.list();

        if(users.size() > 0){
            return users.get(0);
        }else {
            return null;
        }
    }
}