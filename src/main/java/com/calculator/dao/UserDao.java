package com.calculator.dao;

import com.calculator.entity.User;

public interface UserDao {
    public User getUser(String login);
}
