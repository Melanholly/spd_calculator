$(document).ready(function () {
    var screen = $("#calcScreen");
    var result = 0;
    var isFloat = false;
    var needClear = true;
    var operation = "result";

    $('button').click(function () {

        if ($.isNumeric(this.value)) {
            if (needClear) {
                screen.text("");
                needClear = false;
            }
            screen.append(this.value);
        } else if (this.value == "clearEntry") {
            screen.text("0");
            isFloat = false;
            needClear = true;
        } else if (this.value == "dot") {
            if (!isFloat) {
                if (needClear) {
                    screen.text("0");
                    needClear = false;
                }
                screen.append(".");
                isFloat = true;
                needClear = false;
            }
        } else {
            if (needClear) {
                operation = this.value;
            } else if (operation == "result") {
                operation = this.value;
                result = screen.text();
                isFloat = false;
                needClear = true;
            } else {
                var op = this.value;
                $.ajax({
                    type: "post",
                    data: {
                        firstNum: result,
                        secNum: screen.text(),
                        action: operation
                    },
                    success: function (data) {
                        screen.text(data);
                        result = data;
                        operation = op;
                        isFloat = false;
                        needClear = true;
                    }
                });
            }
        }

    });
});