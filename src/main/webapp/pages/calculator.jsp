<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>SPD_Calc</title>
    <link href="<c:url value="/pages/css/style.css" />" rel="stylesheet">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="./pages/js/calculator.js"></script>
</head>
<body>
<div style="width: 200px; border: 1px solid #000;">
    <p  style="width: 150px;
    height: 30px;
    text-align: right;
    border: 10px solid "
        id="calcScreen" >
    </p>



    <form method="post">

        <table style="width: 200px">

            <tr>
                <td>
                    <button type="button"
                            value="7">7</button>
                </td>
                <td>
                    <button type="button"
                            value="8">8</button>
                </td>
                <td>
                    <button type="button"
                            value="9">9</button>
                </td>
                <td>
                    <button type="button"
                            value="div">&divide;</button>
                </td>
                <td>
                    <button type="button"
                            value="C" style= color:red>&#171;</button>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="button"
                            value="4">4</button>
                </td>
                <td>
                    <button type="button"
                            value="5">5</button>
                </td>
                <td>
                    <button type="button"
                            value="6">6</button>
                </td>
                <td>
                    <button type="button"
                            value="mult">&times;</button>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="button"
                            value="1">1</button>
                </td>
                <td>
                    <button type="button"
                            value="2">2</button>
                </td>
                <td>
                    <button type="button"
                            value="3">3</button>
                </td>
                <td>
                    <button type="button"
                            value="minus">-</button>
                </td>

            </tr>
            <tr>
                <td>
                    <button type="button"
                            value="dot">,</button>
                </td>
                <td>
                    <button type="button"
                            value="0">0</button>
                </td>
                <td>
                    <button type="button"
                            value="result" style= color:red>=</button>
                </td>

                <td>
                    <button type="button"
                            value="plus">+</button>
                </td>
            </tr>
        </table>
    </form>


</div>
</body>
</html>