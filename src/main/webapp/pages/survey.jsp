<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
</head>

<body>
<h1>Questions</h1></br>

<form name="survey">
    <c:set var="i" value="1"></c:set>
    <c:forEach var="entry" items="${questions}">
        <div style="padding-bottom: 10px">
            <c:out value="${i}) "></c:out>

            <label>${entry.question}</label></br>

            <c:choose>
                <c:when test="${entry.type == 'text'}">
                    <input type="text">
                </c:when>

                <c:when test="${entry.type == 'radioButton'}">
                    <c:forEach var="answ" items="${entry.answers}">
                        <input type="radio" name="q" value="${answ}">${answ}</br>
                    </c:forEach>
                </c:when>

                <c:when test="${entry.type == 'checkBox'}">
                    <c:forEach var="answ" items="${entry.answers}">
                        <input type="checkbox" name="q" value="${answ}">${answ}</br>
                    </c:forEach>
                </c:when>


            </c:choose>
        </div>
    </c:forEach>

    <input type="button"  value="ok" >
</form>

</body>
</html>